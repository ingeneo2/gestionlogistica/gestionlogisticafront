/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.html',
    './src/**/*.ts',
    './node_modules/@my-company/tailwind-components/**/*.js',
  ],
  theme: {
    extend: {},
  },
  plugins: [ require('@tailwindcss/forms'),],
}

