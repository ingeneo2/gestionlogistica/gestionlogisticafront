import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map, Observable, throwError} from 'rxjs';
import {LoginRequest} from "../models/loginRequest";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) { }

  public login(request: LoginRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/login`, request)
      .pipe(
        map(response => {
          const token = response.token;
          console.log(token)
          localStorage.setItem('jwt_token', token);
          return response;
        }));
  }
}
