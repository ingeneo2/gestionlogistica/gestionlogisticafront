import { Injectable } from '@angular/core';
import {PuertoRequest} from "../models/puertoRequest";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PuertoService {

  constructor(private http: HttpClient) {
  }

  public crearPuerto(request: PuertoRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/api/puertos`, request);
  }
}
