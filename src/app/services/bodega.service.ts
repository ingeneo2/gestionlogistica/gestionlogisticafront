import { Injectable } from '@angular/core';
import {BodegaRequest} from "../models/bodegaRequest";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BodegaService {

  constructor(private http: HttpClient) {
  }
  public crearBodega(request: BodegaRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/api/bodegas`, request);
  }
}
