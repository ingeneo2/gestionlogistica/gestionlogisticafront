import { Injectable } from '@angular/core';
import {ProductoRequest} from "../models/poductoRequest";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient) {
  }
  public crearProducto(request: ProductoRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/api/productos`, request);
  }
}
