import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ClienteRequest} from "../models/clienteRequest";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) {
  }

  public crearCliente(request: ClienteRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/api/clientes`, request);
  }

}
