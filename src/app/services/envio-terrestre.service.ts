import { Injectable } from '@angular/core';
import {EnvioTerrestreRequest} from "../models/envioTerrestreRequest";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EnvioTerrestreService {

  constructor(private http: HttpClient) {
  }

  public crearEnvioTerrestre(request: EnvioTerrestreRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/api/envios/terrestres`, request);
  }
}
