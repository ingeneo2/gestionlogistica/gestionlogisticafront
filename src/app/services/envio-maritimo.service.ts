import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {EnvioMaritimoRequest} from "../models/envioMaritimoRequest";

@Injectable({
  providedIn: 'root'
})
export class EnvioMaritimoService {

  constructor(private http: HttpClient) {
  }

  public crearEnvioMaritomo(request: EnvioMaritimoRequest): Observable<any> {
    console.log(request)
    return this.http.post<any>(`http://localhost:8081/api/envios/maritimos`, request);
  }
}
