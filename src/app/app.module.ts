import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { MenuComponent } from './components/menu/menu.component';
import { CrearClienteComponent } from './components/crear-cliente/crear-cliente.component';
import {ReactiveFormsModule} from "@angular/forms";
import { CrearProductoComponent } from './components/crear-producto/crear-producto.component';
import { CrearPuertoComponent } from './components/crear-puerto/crear-puerto.component';
import { CrearBodegaComponent } from './components/crear-bodega/crear-bodega.component';
import { CrearEnvioTerrestreComponent } from './components/crear-envio-terrestre/crear-envio-terrestre.component';
import { CrearEnvioMaritimoComponent } from './components/crear-envio-maritimo/crear-envio-maritimo.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CrearClienteComponent,
    CrearProductoComponent,
    CrearPuertoComponent,
    CrearBodegaComponent,
    CrearEnvioTerrestreComponent,
    CrearEnvioMaritimoComponent,
    LoginComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
