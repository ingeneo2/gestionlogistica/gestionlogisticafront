import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MenuComponent} from "./components/menu/menu.component";
import {CrearClienteComponent} from "./components/crear-cliente/crear-cliente.component";
import {CrearProductoComponent} from "./components/crear-producto/crear-producto.component";
import {CrearBodegaComponent} from "./components/crear-bodega/crear-bodega.component";
import {CrearPuertoComponent} from "./components/crear-puerto/crear-puerto.component";
import {CrearEnvioTerrestreComponent} from "./components/crear-envio-terrestre/crear-envio-terrestre.component";
import {CrearEnvioMaritimoComponent} from "./components/crear-envio-maritimo/crear-envio-maritimo.component";
import {LoginComponent} from "./components/login/login.component";
import {HomeComponent} from "./components/home/home.component";

const routes: Routes = [

  {
    path:'menu', component: MenuComponent
  },
  {
    path:'crear-cliente', component: CrearClienteComponent
  },
  {
    path:'crear-producto', component: CrearProductoComponent
  },
  {
    path:'crear-bodega', component: CrearBodegaComponent
  },
  {
    path:'crear-puerto', component: CrearPuertoComponent
  },
  {
    path:'crear-envio-terrestre', component: CrearEnvioTerrestreComponent
  },
  {
    path:'crear-envio-maritimo', component: CrearEnvioMaritimoComponent
  },
  {
    path:'login', component: LoginComponent
  },
  {
    path:'', component: HomeComponent
  }
  ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
