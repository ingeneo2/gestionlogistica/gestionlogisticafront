export interface EnvioMaritimoRequest {
  cliente: Cliente;
  producto: Producto;
  puerto: Puerto;
  cantidad: number;
  fechaRegistro: string;
  fechaEntrega: string;
  precioEnvio: string;
  numeroFlota: string;
}

export interface Cliente {
  nombre: string;
}

export interface Producto {
  nombre: string;
}

export interface Puerto {
  nombre: string;
  ubicacion: string;
}
