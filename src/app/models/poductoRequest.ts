export interface ProductoRequest {
  nombre: string;
  descripcion: string;
  tipo: string;
}
