export interface EnvioTerrestreRequest {
  cliente: Cliente;
  producto: Producto;
  bodega: Bodega;
  cantidad: number;
  fechaRegistro: string;
  fechaEntrega: string;
  precioEnvio: string;
  placaVehiculo: string;
}

export interface Cliente {
  nombre: string;
}

export interface Producto {
  nombre: string;
}

export interface Bodega {
  nombre: string;
  ubicacion: string;
}
