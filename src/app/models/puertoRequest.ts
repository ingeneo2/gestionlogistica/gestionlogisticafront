export interface PuertoRequest {
  nombre: string;
  ubicacion: string;
  capacidad: number;
}
