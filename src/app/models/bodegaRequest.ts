export interface BodegaRequest {
  nombre: string;
  ubicacion: string;
  capacidad: number;
}
