import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginService } from '../../services/login.service';
import { LoginRequest } from '../../models/loginRequest';
import {catchError, throwError} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loginForm: FormGroup;
  mensajeError: string = '';
  enviando: boolean = false;
  mostrarErrores: boolean = false;
  errorCredenciales: boolean = false;


  constructor(private router: Router, private fb: FormBuilder, private service: LoginService) {
    this.loginForm = this.fb.group({
      correo: ['', Validators.required],
      contrasena: ['', Validators.required]
    });
  }

  onSubmit() {
    this.mensajeError = '';
    this.enviando = true;

    if (this.loginForm.valid) {
      const valoresFormulario = this.loginForm.value;
      let peticion: LoginRequest = {
        correo: valoresFormulario.correo,
        contrasena: valoresFormulario.contrasena
      };

      this.service.login(peticion).pipe(
        catchError(err => {
          this.enviando = false;
          this.mostrarErrores = true;
          this.errorCredenciales = false;

          if (err.error) {
            this.mensajeError = err.error.error;
             if (err.error.error.includes('password')) {
              this.errorCredenciales = true;
            }
          } else {
            this.mensajeError = 'Error al iniciar sesión. Inténtalo de nuevo.';
          }
          return throwError(err);
        })
      )
        .subscribe({
          next: (response) => {
            console.log(response);
            this.enviando = false;
            this.router.navigate(['/menu']);
          },
          error: (error) => {
            console.error('Error:', error);

            this.errorCredenciales = true;
          }
        });
    } else {
      this.enviando = false;
      this.mostrarErrores = true;
    }
  }



  registrarCliente() {
    this.router.navigate(['/crear-cliente']);
  }
}
