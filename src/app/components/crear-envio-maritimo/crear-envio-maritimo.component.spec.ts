import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEnvioMaritimoComponent } from './crear-envio-maritimo.component';

describe('CrearEnvioMaritimoComponent', () => {
  let component: CrearEnvioMaritimoComponent;
  let fixture: ComponentFixture<CrearEnvioMaritimoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrearEnvioMaritimoComponent]
    });
    fixture = TestBed.createComponent(CrearEnvioMaritimoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
