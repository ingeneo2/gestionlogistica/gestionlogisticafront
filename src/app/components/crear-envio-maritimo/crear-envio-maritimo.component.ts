import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {EnvioMaritimoService} from "../../services/envio-maritimo.service";
import {EnvioMaritimoRequest} from "../../models/envioMaritimoRequest";

@Component({
  selector: 'app-crear-envio-maritimo',
  templateUrl: './crear-envio-maritimo.component.html',
  styleUrls: ['./crear-envio-maritimo.component.css']
})
export class CrearEnvioMaritimoComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(private router: Router, private fb: FormBuilder, private servi: EnvioMaritimoService) {

    this.RegistroForm = this.fb.group({
      nombreCliente: ['', [Validators.required]],
      nombreProducto: ['', [Validators.required]],
      nombrePuerto: ['', [Validators.required]],
      ubicacionPuerto: ['', [Validators.required]],
      cantidad: ['', [Validators.required]],
      fechaRegistro: ['', [Validators.required]],
      fechaEntrega: ['', [Validators.required]],
      precioEnvio: ['', [Validators.required]],
      numeroFlota: ['', [Validators.required]],
    })
    console.log(this.RegistroForm)
  }

  volverMenu() {
    this.router.navigate(['/menu']);
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {

      const formValues = this.RegistroForm.value;
      let request: EnvioMaritimoRequest = {
        cliente: {
          nombre: formValues.nombreCliente
        },
        producto: {
          nombre: formValues.nombreProducto
        },
        puerto: {
          nombre: formValues.nombrePuerto,
          ubicacion: formValues.ubicacionPuerto
        },
        cantidad: formValues.cantidad,
        fechaRegistro: formValues.fechaRegistro,
        fechaEntrega: formValues.fechaEntrega,
        precioEnvio: formValues.precioEnvio,
        numeroFlota: formValues.numeroFlota
      };

      console.log(request)

      this.servi.crearEnvioMaritomo(request).subscribe({
        next: (response) => {
          console.log(response)
          this.respuestaCreacion = response;
          // this.volverMenu();
        },
        error: (error) => {
          console.error('Error en el registro:', error);
          alert("Error al registrar el envio. Por favor, inténtalo de nuevo.");
        }
      });
    } else{
      this.mostrarErrores = true;
    }
  }
}
