import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../../services/login.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  home: FormGroup;
  constructor(private router: Router,private fb: FormBuilder
  ) {
    this.home = this.fb.group({
      userName: ['', Validators.required],
      contrasena: ['', Validators.required]
    });
  }

  crearCuenta() {
    this.router.navigate(['/crear-cliente']);
  }
  login() {
    this.router.navigate(['/login']);
  }
}
