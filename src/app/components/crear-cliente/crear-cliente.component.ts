import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ClienteRequest} from "../../models/clienteRequest";
import {ClienteService} from "../../services/cliente.service";


@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  styleUrls: ['./crear-cliente.component.css']
})
export class CrearClienteComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(private router: Router, private fb: FormBuilder, private servi: ClienteService) {
    this.RegistroForm = this.fb.group({
      nombreCliente: ['', [Validators.required]],
      direccionCliente: ['', [Validators.required]],
      telefonoCliente: ['', [Validators.required]],
      nombreUsuario: ['', [Validators.required]],
      contrasena: ['', [Validators.required]],
      correo: ['', [Validators.required, Validators.email]] // Add email validation
    });
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {
      const formValues = this.RegistroForm.value;
      let request: ClienteRequest = {
        nombre: formValues.nombreCliente,
        direccion: formValues.direccionCliente,
        telefono: formValues.telefonoCliente,
        nombreUsuario: formValues.nombreUsuario,
        contrasena: formValues.contrasena,
        correo: formValues.correo
      };

      this.servi.crearCliente(request).subscribe({
        next: (response) => {
          console.log(response);
          this.respuestaCreacion = response;
          // this.volverMenu(); // Assuming you handle navigation elsewhere
        },
        error: (error) => {
          console.error('Error en el registro:', error);
          alert("Error al registrar el cliente. Por favor, inténtalo de nuevo.");
          this.mostrarErrores = true;
        }
      });
    } else {
      this.mostrarErrores = true;
    }
  }


  volverMenu() {
    this.router.navigate(['/menu']);
  }
  volverLogin() {
    this.router.navigate(['']);
  }
}
