import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEnvioTerrestreComponent } from './crear-envio-terrestre.component';

describe('CrearEnvioTerrestreComponent', () => {
  let component: CrearEnvioTerrestreComponent;
  let fixture: ComponentFixture<CrearEnvioTerrestreComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrearEnvioTerrestreComponent]
    });
    fixture = TestBed.createComponent(CrearEnvioTerrestreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
