import { Component } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {EnvioTerrestreService} from "../../services/envio-terrestre.service";
import {EnvioTerrestreRequest} from "../../models/envioTerrestreRequest";

@Component({
  selector: 'app-crear-envio-terrestre',
  templateUrl: './crear-envio-terrestre.component.html',
  styleUrls: ['./crear-envio-terrestre.component.css']
})
export class CrearEnvioTerrestreComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(private router: Router, private fb: FormBuilder, private servi: EnvioTerrestreService) {

    this.RegistroForm = this.fb.group({
      nombreCliente: ['', [Validators.required]],
      nombreProducto: ['', [Validators.required]],
      nombreBodega: ['', [Validators.required]],
      ubicacionBodega: ['', [Validators.required]],
      cantidad: ['', [Validators.required]],
      fechaRegistro: ['', [Validators.required]],
      fechaEntrega: ['', [Validators.required]],
      precioEnvio: ['', [Validators.required]],
      placaVehiculo: ['', [Validators.required]],
    })
    console.log(this.RegistroForm)
  }

  volverMenu() {
    this.router.navigate(['/menu']);
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {

      const formValues = this.RegistroForm.value;
      let request: EnvioTerrestreRequest = {
        cliente: {
          nombre: formValues.nombreCliente
        },
        producto: {
          nombre: formValues.nombreProducto
        },
        bodega: {
          nombre: formValues.nombreBodega,
          ubicacion: formValues.ubicacionBodega
        },
        cantidad: formValues.cantidad,
        fechaRegistro: formValues.fechaRegistro,
        fechaEntrega: formValues.fechaEntrega,
        precioEnvio: formValues.precioEnvio,
        placaVehiculo: formValues.placaVehiculo
      };

      console.log(request)

      this.servi.crearEnvioTerrestre(request).subscribe({
        next: (response) => {
          this.respuestaCreacion = response;
          // this.volverMenu();
        },
        error: (error) => {
          console.error('Error en el registro:', error);
          alert("Error al registrar el envio. Por favor, inténtalo de nuevo.");
        }
      });
    } else{
      this.mostrarErrores = true;
    }
  }
}
