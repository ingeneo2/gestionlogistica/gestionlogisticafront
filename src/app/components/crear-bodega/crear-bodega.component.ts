import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {BodegaRequest} from "../../models/bodegaRequest";
import {BodegaService} from "../../services/bodega.service";

@Component({
  selector: 'app-crear-bodega',
  templateUrl: './crear-bodega.component.html',
  styleUrls: ['./crear-bodega.component.css']
})
export class CrearBodegaComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(private router: Router, private fb: FormBuilder, private servi: BodegaService) {

    this.RegistroForm = this.fb.group({
      nombreBodega: ['', [Validators.required]],
      ubicacion: ['', [Validators.required]],
      capacidad: ['', [Validators.required]],
    })
  }

  volverMenu() {
    this.router.navigate(['/menu']);
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {

      const formValues = this.RegistroForm.value;
      let request: BodegaRequest = {
        nombre: formValues.nombreBodega,
        ubicacion: formValues.ubicacion,
        capacidad: formValues.capacidad
      };
      console.log(request)

      this.servi.crearBodega(request).subscribe({
        next: (response) => {
          this.respuestaCreacion = response;
          this.volverMenu();
        },
        error: (error) => {
          console.error('Error en el registro:', error);
        }
      });
    }
  }
}
