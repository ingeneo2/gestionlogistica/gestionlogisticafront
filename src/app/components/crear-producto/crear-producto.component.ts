import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ProductoRequest} from "../../models/poductoRequest";
import {ProductoService} from "../../services/producto.service";


@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(private router: Router, private fb: FormBuilder, private servi: ProductoService) {

    this.RegistroForm = this.fb.group({
      nombreProducto: ['', [Validators.required]],
      descripcionProducto: ['', [Validators.required]],
      tipoProducto: ['', [Validators.required]],
    })
  }

  volverMenu() {
    this.router.navigate(['/menu']);
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {

      const formValues = this.RegistroForm.value;
      let request: ProductoRequest = {
        nombre: formValues.nombreProducto,
        descripcion: formValues.descripcionProducto,
        tipo: formValues.tipoProducto
      };
      console.log(request)

      this.servi.crearProducto(request).subscribe({
        next: (response) => {
          this.respuestaCreacion = response;
         // this.volverMenu();
        },
        error: (error) => {
          console.error('Error en el registro:', error);
        }
      });
    }
  }
}
