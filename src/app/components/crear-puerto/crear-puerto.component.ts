import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {PuertoRequest} from "../../models/puertoRequest";
import {PuertoService} from "../../services/puerto.service";

@Component({
  selector: 'app-crear-puerto',
  templateUrl: './crear-puerto.component.html',
  styleUrls: ['./crear-puerto.component.css']
})
export class CrearPuertoComponent {
  RegistroForm: FormGroup;
  mostrarErrores: boolean = false;
  respuestaCreacion: any;

  constructor(private router: Router, private fb: FormBuilder, private servi: PuertoService) {

    this.RegistroForm = this.fb.group({
      nombrePuerto: ['', [Validators.required]],
      ubicacion: ['', [Validators.required]],
      capacidad: ['', [Validators.required]],
    })
  }

  volverMenu() {
    this.router.navigate(['/menu']);
  }

  guardar() {
    this.mostrarErrores = true;

    if (this.RegistroForm.valid) {

      const formValues = this.RegistroForm.value;
      let request: PuertoRequest = {
        nombre: formValues.nombrePuerto,
        ubicacion: formValues.ubicacion,
        capacidad: formValues.capacidad
      };
      console.log(request)

      this.servi.crearPuerto(request).subscribe({
        next: (response) => {
          this.respuestaCreacion = response;
          // this.volverMenu();
        },
        error: (error) => {
          console.error('Error en el registro:', error);
        }
      });
    }
  }
}
