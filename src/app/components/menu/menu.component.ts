import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {



  constructor(private router: Router) {

  }

  registrarPuerto() {
    this.router.navigate(['/crear-puerto']);
  }
  registrarBodega() {
    this.router.navigate(['/crear-bodega']);
  }
  registrarProducto() {
    this.router.navigate(['/crear-producto']);
  }
  registrarEnvioMaritimo() {
    this.router.navigate(['/crear-envio-maritimo']);
  }
  registrarEnvioTerrestre() {
    this.router.navigate(['/crear-envio-terrestre']);
  }
  cerrarSesion() {
    this.router.navigate(['']);
  }
}
